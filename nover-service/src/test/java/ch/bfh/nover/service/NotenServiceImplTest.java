package ch.bfh.nover.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.ModuleSummary;
import ch.bfh.nover.domain.service.BolognaMark;

/**
 * Unit test for simple App.
 */
public class NotenServiceImplTest {
    
    private final NotenServiceImpl notenService;
    
    
    public NotenServiceImplTest() {
       notenService = new NotenServiceImpl(null); // TODO: guice injection
    }
    
    @Test
    public void testGetModuleSummary() {
        // given
        Module module = new Module();
        Mark mark1 = new Mark("test1", "A", 50);
        Mark mark2 = new Mark("test2", "A", 50);
        module.setMarks(Arrays.asList(mark1, mark2));
        
        // call
        ModuleSummary moduleSummary = notenService.getModuleSummary(module);

        // test
        assertEquals(BolognaMark.A.name(), moduleSummary.getMark());
        assertTrue(moduleSummary.isSufficent());
    }
    
    @Test
    public void testGetModuleSummaryWithB() {
        // given
        Module module = new Module();
        Mark mark1 = new Mark("test1", "B", 50);
        Mark mark2 = new Mark("test2", "B", 50);
        module.setMarks(Arrays.asList(mark1, mark2));
        
        // call
        ModuleSummary moduleSummary = notenService.getModuleSummary(module);

        // test
        assertEquals(BolognaMark.B.name(), moduleSummary.getMark());
        assertTrue(moduleSummary.isSufficent());
    }
}
