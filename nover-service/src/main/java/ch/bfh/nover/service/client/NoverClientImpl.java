package ch.bfh.nover.service.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.Student;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerRequest;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;

public class NoverClientImpl implements NoverClient {
    private static final int SERVER_PORT = 9000;

    private static final String SERVER_HOST = "localhost";

    private final Gson gson;

    public NoverClientImpl() {
        gson = new Gson();
    }


    @Override
    public Semester addSemester(Credentials credentials, Semester semester) {

        System.out.println("calling addSemester. semester:=" + semester);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.ADD_SEMESTER);
        request.setPayload(gson.toJson(semester));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Semester addedSemester = (Semester)gson.fromJson(response.getPayload(),
                    Method.ADD_SEMESTER.getResponseClazz());
            System.out.println("Returning semester:= " + addedSemester);
            return addedSemester;
        }

        return null;

    }

    @Override
    public Student login(Credentials credentials) {

        System.out.println("calling login. credentials:=" + credentials);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.LOGIN);
        request.setCredentials(credentials);
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Student loggedInStudent = (Student)gson.fromJson(response.getPayload(), Method.LOGIN.getResponseClazz());
            System.out.println("Returning student:= " + loggedInStudent);
            return loggedInStudent;
        }

        return null;
    }

    @Override
    public Student registerStudent(Student student) {
        System.out.println("calling registerStudent. student:=" + student);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.REGISTER_STUDENT);
        request.setPayload(new Gson().toJson(student));

        // server call
        ServerResponse response = callServer(request);

        // process response
        boolean success = checkResponseStatus(response);

        if (success) {
            // deserialize
            String payload = response.getPayload();
            Student createdStudent = (Student)gson.fromJson(payload, Method.REGISTER_STUDENT.getResponseClazz());

            System.out.println("Returning student:= " + student);
            return createdStudent;
        }

        return null;
    }

    @Override
    public Mark addMark(Credentials credentials, Mark mark) {
        System.out.println("calling addMark. mark:=" + mark);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.ADD_MARK);
        request.setPayload(gson.toJson(mark));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Mark addedMark = (Mark)gson.fromJson(response.getPayload(), Method.ADD_MARK.getResponseClazz());
            System.out.println("Returning semester:= " + addedMark);
            return addedMark;
        }

        return null;
    }

    @Override
    public Module addModule(Credentials credentials, Module module) {
        System.out.println("calling addModule. module:=" + module);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.ADD_MODULE);
        request.setPayload(gson.toJson(module));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Module addedModule = (Module)gson.fromJson(response.getPayload(), Method.ADD_MODULE.getResponseClazz());
            System.out.println("Returning semester:= " + addedModule);
            return addedModule;
        }

        return null;
    }

    @Override
    public boolean deleteStudent(Credentials credentials, Long studentId) {
        System.out.println("calling deleteStudent. studentId:=" + studentId);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.DELETE_STUDENT);
        request.setPayload(gson.toJson(studentId));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Boolean deleteSuccess = (Boolean)gson.fromJson(response.getPayload(),
                    Method.DELETE_STUDENT.getResponseClazz());
            System.out.println("Returning:= " + deleteSuccess);
            return deleteSuccess;
        }

        return false;
    }

    @Override
    public boolean deleteSemester(Credentials credentials, Long semesterId) {
        System.out.println("calling deleteSemester. semesterId:=" + semesterId);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.DELETE_SEMESTER);
        request.setPayload(gson.toJson(semesterId));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Boolean deleteSuccess = (Boolean)gson.fromJson(response.getPayload(), Method.DELETE_SEMESTER.getResponseClazz());
            System.out.println("Returning:= " + deleteSuccess);
            return deleteSuccess;
        }

        return false;
    }

    @Override
    public boolean deleteModule(Credentials credentials, Long moduleId) {
        System.out.println("calling deleteModule. moduleId:=" + moduleId);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.DELETE_MODULE);
        request.setPayload(gson.toJson(moduleId));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Boolean deleteSuccess = (Boolean)gson.fromJson(response.getPayload(),
                    Method.DELETE_MODULE.getResponseClazz());
            System.out.println("Returning:= " + deleteSuccess);
            return deleteSuccess;
        }

        return false;
    }

    @Override
    public boolean deleteMark(Credentials credentials, Long markId) {
        System.out.println("calling deleteMark. markId:=" + markId);

        ServerRequest request = new ServerRequest();
        request.setMethod(Method.DELETE_MARK);
        request.setPayload(gson.toJson(markId));
        request.setCredentials(credentials);

        ServerResponse response = callServer(request);

        boolean success = checkResponseStatus(response);

        if (success) {
            Boolean deleteSuccess = (Boolean)gson.fromJson(response.getPayload(),
                    Method.DELETE_MARK.getResponseClazz());
            System.out.println("Returning:= " + deleteSuccess);
            return deleteSuccess;
        }

        return false;
    }

    private boolean checkResponseStatus(ServerResponse response) {
        String defaultErrMsg = "Error calling nover server: ";

        if (response == null) {
            System.err.println(defaultErrMsg + "response is null");
            return false;
        } else if (Status.ERROR.equals(response.getStatus())) {
            System.err.println(defaultErrMsg + " status:=" + response.getStatus() + " msg:=" + response.getPayload());
            return false;
        } else if (Status.OK.equals(response.getStatus())) {
            return true;
        }

        System.err.println(defaultErrMsg + "Unknown error");
        return false;
    }

    private ServerResponse callServer(ServerRequest request) {
        String response;
        Socket clientSocket = null;
        ServerResponse serverResponse = null;

        try {
            String requestPayload = new Gson().toJson(request);
            clientSocket = new Socket(SERVER_HOST, SERVER_PORT);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outToServer.writeBytes(requestPayload + '\n');
            response = inFromServer.readLine();
            System.out.println("Response from Server: " + response);

            if (StringUtils.isNotBlank(response)) {
                System.out.println("Getting response of Server. response:=" + response);
                serverResponse = new Gson().fromJson(response, ServerResponse.class);
            } else {
                System.err.println("Problem calling nover server. Response is empty");
            }

            return serverResponse;
        } catch (IOException ex) {
            String errorMsg = "Error while establishing connection to nover server host:=" + SERVER_HOST + ", port:="
                    + SERVER_PORT;
            System.err.print(errorMsg);
            serverResponse = new ServerResponse();
            serverResponse.setStatus(Status.ERROR);
            serverResponse.setPayload(errorMsg);
        } finally {
            if (clientSocket != null && !clientSocket.isClosed()) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        }
        return serverResponse;
    }

}
