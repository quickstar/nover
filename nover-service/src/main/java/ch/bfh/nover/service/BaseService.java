package ch.bfh.nover.service;

import ch.bfh.nover.service.client.NoverClient;

public abstract class BaseService {
    
    protected NoverClient noverClient;

    public BaseService(NoverClient noverClient) {
        this.noverClient = noverClient;
    }
}
