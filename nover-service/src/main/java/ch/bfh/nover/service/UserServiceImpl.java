package ch.bfh.nover.service;


import com.google.inject.Inject;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Student;
import ch.bfh.nover.service.client.NoverClient;

public class UserServiceImpl extends BaseService implements UserService {
    @Inject
    public UserServiceImpl(NoverClient noverClient) {
        super(noverClient);
    }

    @Override
    public Student register(Student student) {
        return noverClient.registerStudent(student);
        
    }

    @Override
    public Student login(Credentials credentials) {
        return noverClient.login(credentials);
    }

    @Override
    public boolean deleteStudent(Credentials credentials, Long studentId) {
        return noverClient.deleteStudent(credentials, studentId);
    }
}
