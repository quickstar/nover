package ch.bfh.nover.service;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Student;

public interface UserService {

    Student register(Student user);

    Student login(Credentials credentials);

    boolean deleteStudent(Credentials credentials, Long studentId);
}
