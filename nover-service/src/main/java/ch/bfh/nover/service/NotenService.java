package ch.bfh.nover.service;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.ModuleSummary;
import ch.bfh.nover.domain.Semester;

public interface NotenService {
    
    Semester addSemester(Credentials credentials, Semester semester);
    
    Module addModule(Credentials credentials, Module module);

    Mark addMark(Credentials credentials, Mark mark);

    ModuleSummary getModuleSummary(Module module);
    
    boolean deleteSemester(Credentials credentials, Long semesterId);
    
    boolean deleteModule(Credentials credentials, Long moduleId);
    
    boolean deleteMark(Credentials credentials, Long markId);
    
}
