package ch.bfh.nover.service.client;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.Student;

public interface NoverClient {
    
    Student registerStudent(Student student);
    
    Student login(Credentials credentials);
    
    Semester addSemester(Credentials credentials, Semester semester);
    
    Mark addMark(Credentials credentials,  Mark mark);
    
    Module addModule(Credentials credentials, Module module);
    
    boolean deleteStudent(Credentials credentials, Long studentId);
    
    boolean deleteSemester(Credentials credentials, Long semesterId);
    
    boolean deleteModule(Credentials credentials, Long moduleId);
    
    boolean deleteMark(Credentials credentials, Long markId);
}
