package ch.bfh.nover.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.ModuleSummary;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.service.BolognaMark;
import ch.bfh.nover.service.client.NoverClient;

@Singleton
public class NotenServiceImpl extends BaseService implements NotenService {

    @Inject
    public NotenServiceImpl(NoverClient noverClient) {
        super(noverClient);
    }

    @Override
    public Semester addSemester(Credentials credentials, Semester semester) {
        return noverClient.addSemester(credentials, semester);
    }

    @Override
    public Module addModule(Credentials credentials, Module module) {
        return noverClient.addModule(credentials, module);
    }

    @Override
    public Mark addMark(Credentials credentials, Mark mark) {
        return noverClient.addMark(credentials, mark);
    }

    @Override
    public ModuleSummary getModuleSummary(Module module) {

        List<Mark> marks = module.getMarks();
        ModuleSummary summary = new ModuleSummary();
        
        if (marks != null) {
            List<Mark> marksToCalcutate = marks.stream().filter(m -> StringUtils.isNotBlank(m.getMark()))
                    .collect(Collectors.toList());
            
            float mult = 0.01f;
            float sum = 0;
            
            BolognaMark resultBologna = BolognaMark.F;
            
            for (Mark mark : marksToCalcutate) {
                String markValue = mark.getMark();
                BolognaMark bolognaMark = BolognaMark.valueOf(markValue);
                Integer validity = mark.getValidity();
                sum += ((bolognaMark.getPercentage() * mult) * (validity * mult));
            }
            
            sum = sum * 100;
            
            if (sum < BolognaMark.F.getPercentage()) {
                resultBologna = BolognaMark.F;
            } else if (sum > BolognaMark.F.getPercentage() && sum < BolognaMark.E.getPercentage()) {
                resultBologna = BolognaMark.F;
            } else if (sum >= BolognaMark.E.getPercentage() && sum < BolognaMark.D.getPercentage()) {
                resultBologna = BolognaMark.E;
            } else if (sum >= BolognaMark.D.getPercentage() && sum < BolognaMark.C.getPercentage()) {
                resultBologna = BolognaMark.D;
            } else if (sum >= BolognaMark.C.getPercentage() && sum < BolognaMark.B.getPercentage()) {
                resultBologna = BolognaMark.C;
            } else if (sum >= BolognaMark.B.getPercentage() && sum < BolognaMark.A.getPercentage()) {
                resultBologna = BolognaMark.B;
            } else {
                resultBologna = BolognaMark.A;
            }
            summary.setMark(resultBologna.name());
            summary.setSufficent(!BolognaMark.F.equals(resultBologna));
        }
        return summary;
    }

    @Override
    public boolean deleteSemester(Credentials credentials, Long semesterId) {
        return noverClient.deleteSemester(credentials, semesterId);
    }

    @Override
    public boolean deleteModule(Credentials credentials, Long moduleId) {
        return noverClient.deleteModule(credentials, moduleId);
    }

    @Override
    public boolean deleteMark(Credentials credentials, Long markId) {
        return noverClient.deleteMark(credentials, markId);
    }
}
