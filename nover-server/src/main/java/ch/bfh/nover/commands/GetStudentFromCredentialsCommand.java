package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Student;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;
import com.google.gson.Gson;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public class GetStudentFromCredentialsCommand implements Command
{
    private Credentials _credentials;
    private PersistanceManager _persistanceManager;

    public GetStudentFromCredentialsCommand(Credentials credentials, PersistanceManager persistanceManager)
    {
        _credentials = credentials;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
    {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.LOGIN);

        try
        {
            Student student = _persistanceManager.findStudentByCredentials(_credentials);
            if (student != null) {
                response.setPayload(new Gson().toJson(student));
            }

        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setPayload(ex.getMessage());
        }
        return response;
    }
}
