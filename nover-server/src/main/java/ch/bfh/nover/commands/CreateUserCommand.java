package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Student;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;
import ch.bfh.nover.service.UserService;
import com.google.gson.Gson;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public class CreateUserCommand implements Command
{
    private Student _student;
    private PersistanceManager _persistanceManager;

    public CreateUserCommand(Student student, PersistanceManager persistanceManager)
    {
        _student = student;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
    {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.REGISTER_STUDENT);

        try
        {
            Student student = _persistanceManager.save(_student);
            if (student != null) {
                response.setPayload(new Gson().toJson(student));
            }

        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setPayload(ex.getMessage());
        }
        return response;
    }
}
