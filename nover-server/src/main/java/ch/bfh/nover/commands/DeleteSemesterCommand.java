package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;
import com.google.gson.Gson;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public class DeleteSemesterCommand implements Command
{
    private Credentials _credentials;
    private Long _semesterId;
    private PersistanceManager _persistanceManager;

    public DeleteSemesterCommand(Credentials credentials, Long semesterId, PersistanceManager persistanceManager)
    {
        _credentials = credentials;
        _semesterId = semesterId;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
    {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.ADD_SEMESTER);
        boolean deleteSuccessfull = false;

        try
        {
            deleteSuccessfull = _persistanceManager.deleteSemester(_semesterId);

        }
        catch (Exception ex) {
            response.setStatus(Status.ERROR);
        }
        response.setPayload(new Gson().toJson(deleteSuccessfull));
        return response;
    }
}
