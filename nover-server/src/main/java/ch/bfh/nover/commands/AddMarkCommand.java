package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;
import com.google.gson.Gson;

public class AddMarkCommand implements Command
{
    private Credentials _credentials;
    private Mark _mark;
    private PersistanceManager _persistanceManager;

    public AddMarkCommand(Credentials credentials, Mark mark, PersistanceManager persistanceManager)
    {
        _credentials = credentials;
        _mark = mark;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
        {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.ADD_MARK);

        try
        {
            Mark mark = _persistanceManager.save(_mark);
            if (mark != null) {
                response.setPayload(new Gson().toJson(mark));
            }

        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setPayload(ex.getMessage());
        }
        return response;
    }
}
