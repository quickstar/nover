package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManagerImpl;
import ch.bfh.nover.domain.*;
import ch.bfh.nover.domain.server.Method;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public final class CommandFactory
{
    public static Command GetCommmand(Method method, Object currentObj, Credentials credentials)
    {
        PersistanceManagerImpl persistanceManager = new PersistanceManagerImpl();
        persistanceManager.setCredentials(credentials);

        switch (method){
            case REGISTER_STUDENT:
                return new CreateUserCommand((Student)currentObj, persistanceManager);
            case LOGIN:
                return new GetStudentFromCredentialsCommand(credentials, persistanceManager);
            case ADD_SEMESTER:
                return new AddSemesterCommand(credentials, (Semester)currentObj, persistanceManager);
            case DELETE_SEMESTER:
                return new DeleteSemesterCommand(credentials, (Long)currentObj, persistanceManager);
            case ADD_MODULE:
                return new AddModuleCommand(credentials, (Module)currentObj, persistanceManager);
            case DELETE_MODULE:
                return new DeleteModuleCommand(credentials, (Long)currentObj, persistanceManager);
            case ADD_MARK:
                return new AddMarkCommand(credentials, (Mark)currentObj, persistanceManager);
            case DELETE_MARK:
                return new DeleteMarkCommand(credentials, (Long)currentObj, persistanceManager);
            default:
                return null;
        }
    }
}
