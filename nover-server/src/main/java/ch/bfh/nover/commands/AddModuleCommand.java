package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;
import com.google.gson.Gson;

public class AddModuleCommand implements Command
{
    private Credentials _credentials;
    private Module _module;
    private PersistanceManager _persistanceManager;

    public AddModuleCommand(Credentials credentials, Module module, PersistanceManager persistanceManager)
    {
        _credentials = credentials;
        _module = module;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
        {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.ADD_MODULE);

        try
        {
            Module module = _persistanceManager.save(_module);
            if (module != null) {
                response.setPayload(new Gson().toJson(module));
            }

        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setPayload(ex.getMessage());
        }
        return response;
    }
}
