package ch.bfh.nover.commands;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.Student;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;
import com.google.gson.Gson;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public class AddSemesterCommand implements Command
{
    private Credentials _credentials;
    private Semester _semester;
    private PersistanceManager _persistanceManager;

    public AddSemesterCommand(Credentials credentials, Semester semester, PersistanceManager persistanceManager)
    {
        _credentials = credentials;
        _semester = semester;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
        {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.ADD_SEMESTER);

        try
        {
            Semester semester = _persistanceManager.save(_semester);
            if (semester != null) {
                response.setPayload(new Gson().toJson(semester));
            }

        } catch (Exception ex) {
            response.setStatus(Status.ERROR);
            response.setPayload(ex.getMessage());
        }
        return response;
    }
}
