package ch.bfh.nover.commands;

import com.google.gson.Gson;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerResponse;
import ch.bfh.nover.domain.server.Status;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public class DeleteMarkCommand implements Command
{
    private Credentials _credentials;
    private Long _markId;
    private PersistanceManager _persistanceManager;

    public DeleteMarkCommand(Credentials credentials, Long markId, PersistanceManager persistanceManager)
    {
        _credentials = credentials;
        _markId = markId;
        _persistanceManager = persistanceManager;
    }

    @Override
    public ServerResponse execute()
    {
        ServerResponse response = new ServerResponse();
        response.setStatus(Status.OK);
        response.setMethod(Method.DELETE_MARK);
        boolean deleteSuccessfull = false;

        try
        {
            deleteSuccessfull = _persistanceManager.deleteMark(_markId);

        }
        catch (Exception ex) {
            response.setStatus(Status.ERROR);
        }
        response.setPayload(new Gson().toJson(deleteSuccessfull));
        return response;
    }
}
