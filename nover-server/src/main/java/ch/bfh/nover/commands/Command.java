package ch.bfh.nover.commands;

import ch.bfh.nover.domain.server.ServerResponse;

/**
 * Created by lukas_000 on 28.12.2015.
 */
public interface Command {
    ServerResponse execute();
}
