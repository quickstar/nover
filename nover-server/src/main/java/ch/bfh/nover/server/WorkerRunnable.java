package ch.bfh.nover.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

import ch.bfh.nover.commands.Command;
import ch.bfh.nover.commands.CommandFactory;
import ch.bfh.nover.domain.server.Method;
import ch.bfh.nover.domain.server.ServerRequest;
import ch.bfh.nover.domain.server.ServerResponse;

/**
 * Created by Lukas on 28.12.2015.
 */
public class WorkerRunnable implements Runnable {
    protected Socket _clientSocket = null;
    protected String _serverText = null;
    Gson _serializer = new Gson();

    public WorkerRunnable(Socket clientSocket, String serverText)
    {
        this._clientSocket = clientSocket;
        this._serverText = serverText;
    }

    public void run()
    {
        try
        {
            InputStream input  = _clientSocket.getInputStream();
            OutputStream output = _clientSocket.getOutputStream();

            PrintWriter out = new PrintWriter(output, true);

            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(input));
            String currentLine;

            while((currentLine = inFromClient.readLine()) != null)
            {
                System.out.println("reading data from client: " + currentLine);
                ServerRequest serverRequest = _serializer.fromJson(currentLine, ServerRequest.class);
                Method method = serverRequest.getMethod();
                Object currentObj = _serializer.fromJson(serverRequest.getPayload(), method.getRequestClazz());

                Command command = CommandFactory.GetCommmand(method, currentObj, serverRequest.getCredentials());
                ServerResponse serverResponse = command.execute();

                out.println(_serializer.toJson(serverResponse));
            }
            output.close();
            input.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
