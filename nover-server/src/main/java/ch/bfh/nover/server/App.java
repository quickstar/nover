package ch.bfh.nover.server;

import java.util.Scanner;

/**
 * Hello world!
 */
public class App {
    private static Thread _thread = null;
    private static MultiThreadedServer _server = null;

    public static void main(String[] args) {
        StartServerInstance();

        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            if (in.nextLine().equals("x")){
                break;
            }
        }

        ShutdownServer();
    }

    private static void ShutdownServer() {
        _thread.interrupt();
        _server.stop();
    }

    private static void StartServerInstance() {
        System.out.println("Starting the server...");
        _server = new MultiThreadedServer();
        _thread = new Thread(_server);
        _thread.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        System.out.println("Hit \"x\" to terminate the server");
    }
}

