package ch.bfh.nover.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Lukas on 28.12.2015.
 */
public class MultiThreadedServer implements Runnable {
    protected int _serverPort = 9000;
    protected ServerSocket _serverSocket = null;
    protected Thread _currentThread = null;

    public MultiThreadedServer(int port) {
        this._serverPort = port;
    }

    public MultiThreadedServer() {
    }

    public void run() {
        synchronized (this) {
            this._currentThread = Thread.currentThread();
        }

        openServerSocket();
        while (!_currentThread.isInterrupted()) {
            Socket clientSocket = null;

            try {
                System.out.println("Waiting for requests...");
                clientSocket = this._serverSocket.accept();
            } catch (IOException e) {
                if (_currentThread.isInterrupted()) {
                    System.out.println("Server Stopped.");
                    return;
                }
                throw new RuntimeException("Error accepting client connection", e);
            }
            new Thread(new WorkerRunnable(clientSocket, "Multithreaded Server")).start();
        }
        stop();
    }

    public synchronized void stop() {
        try {
            System.out.println("try to close the server listening on port: " + _serverPort);
            this._serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        System.out.println("Opening socket on port: " + _serverPort);
        try {
            this._serverSocket = new ServerSocket(this._serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port " + _serverPort, e);
        }
    }
}
