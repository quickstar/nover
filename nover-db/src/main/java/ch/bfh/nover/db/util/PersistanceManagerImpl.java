package ch.bfh.nover.db.util;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.Student;

public class PersistanceManagerImpl implements PersistanceManager {
    EntityManager em;
    private Credentials _credentials;

    public PersistanceManagerImpl() {
        em = NoverPersistance.INSTANCE.getEntityManager();
    }
    
    @Override
    public Student save(Student student) {
        saveEntity(student);
        return student;
    }

    @Override
    public Mark save(Mark mark) {
        saveEntity(mark);
        return mark;
    }

    @Override
    public Semester save(Semester semester) {
        saveEntity(semester);
        return semester;
    }

    @Override
    public Module save(Module module) {
        saveEntity(module);
        return module;
    }

    private void saveEntity(Object object) {
        em.getTransaction().begin();
        em.persist(object);
        em.getTransaction().commit();
    }

    @Override
    public Student findStudentByCredentials(Credentials credentials) {
        Query query = em.createQuery("SELECT s FROM Student s WHERE username = :username and password = :password");
        query.setParameter("username", credentials.getUsername());
        query.setParameter("password", credentials.getPassword());
        return (Student) query.getSingleResult();
    }

    @Override
    public void setCredentials(Credentials credentials) {
        _credentials = credentials;
    }

    @Override
    public boolean deleteStudent(Long studentId) {
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE FROM Student WHERE studentId = :studentId");
        query.setParameter("studentId", studentId);
        query.executeUpdate();
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean deleteSemester(Long semesterId) {
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE FROM Semester WHERE semesterId = :semesterId");
        query.setParameter("semesterId", semesterId);
        query.executeUpdate();
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean deleteModule(Long moduleId) {
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE FROM Module WHERE moduleId = :moduleId");
        query.setParameter("moduleId", moduleId);
        query.executeUpdate();
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean deleteMark(Long markId) {
        em.getTransaction().begin();
        Query query = em.createQuery("DELETE FROM Mark WHERE markId = :markId");
        query.setParameter("markId", markId);
        query.executeUpdate();
        em.getTransaction().commit();
        return true;
    }
}
