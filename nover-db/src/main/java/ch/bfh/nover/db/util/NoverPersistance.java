package ch.bfh.nover.db.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public enum NoverPersistance {
    INSTANCE;
    private EntityManagerFactory emFactory;

    private NoverPersistance() {
        emFactory = Persistence.createEntityManagerFactory("nover");
    }

    public EntityManager getEntityManager() {
        return emFactory.createEntityManager();
    }

    public void close() {
        emFactory.close();
    }
}
