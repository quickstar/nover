package ch.bfh.nover.db.util;

import ch.bfh.nover.domain.*;

public interface PersistanceManager {

    Student save(Student student);

    Mark save(Mark mark);
    
    Semester save(Semester semester);

    Module save(Module module);

    Student findStudentByCredentials(Credentials credentials);

    void setCredentials(Credentials credentials);
    
    boolean deleteStudent(Long studentId);
    
    boolean deleteSemester(Long semesterId);
    
    boolean deleteModule(Long moduleId);

    boolean deleteMark(Long markId);
}
