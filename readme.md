```shell
$$\   $$\  $$$$$$\  $$\    $$\ $$$$$$$$\ $$$$$$$\  
$$$\  $$ |$$  __$$\ $$ |   $$ |$$  _____|$$  __$$\ 
$$$$\ $$ |$$ /  $$ |$$ |   $$ |$$ |      $$ |  $$ |
$$ $$\$$ |$$ |  $$ |\$$\  $$  |$$$$$\    $$$$$$$  |
$$ \$$$$ |$$ |  $$ | \$$\$$  / $$  __|   $$  __$$< 
$$ |\$$$ |$$ |  $$ |  \$$$  /  $$ |      $$ |  $$ |
$$ | \$$ | $$$$$$  |   \$  /   $$$$$$$$\ $$ |  $$ |
\__|  \__| \______/     \_/    \________|\__|  \__|
```

## Projekt Builden
1. im root des projekts "mvn clean install package" ausführen
2. im im verzeichnis "./nover-ui/target" befindet sich das ausführbare jar file (with dependencies)
3. mit "java -jar nover-ui-{version}-jar-with-dependencies.jar" UI starten

Neues Modul hinzufügen
--------------------------------------------------
1. Ins root verzeichnis des projekts wechseln
2. "mvn archetype:create -DgroupId=ch.bfh.nover.{MODUL_NAME} -DartifactId=nover-{MODUL_NAME}" ausführen
3. Maven Projekt in IDE importieren

Datenbank aufsetzen
--------------------------------------------------
1. MySQL Server lokal installieren
2. /nover-db/src/main/resources/scripts/init.sql ausführen
