package ch.bfh.nover.ui.beans;

import java.util.List;

import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.Semester;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SemesterBean extends EntityBean<Semester> {

    private LongProperty semesterIdProperty;

    private LongProperty studentIdProperty;

    private StringProperty semesterNameProperty;

    private ObservableList<ModuleBean> moduleData = FXCollections.observableArrayList();

    public SemesterBean() {
        setEntity(new Semester());
    }

    public SemesterBean(Semester semester) {
        setEntity(semester);
    }

    public SemesterBean(String semesterName) {
        setEntity(new Semester(semesterName));
    }

    // SemesterName Getters and Setters
    public void setSemesterName(String firstName) {
        this.semesterNameProperty.set(firstName);
    }

    public String getSemesterName() {
        return semesterNameProperty.get();
    }

    public StringProperty getSemesterNameProperty() {
        return semesterNameProperty;
    }

    // Semester Methods
    public ObservableList<ModuleBean> getModuleData() {
        return moduleData;
    }

    public void addNewModule(ModuleBean moduleBean) {
        moduleData.add(moduleBean);
    }

    public void removeModule(int index) {
        moduleData.remove(index);
    }

    @Override
    public Semester updateEntity() {
        Semester entity = getEntity();
        entity.setSemesterId(this.semesterIdProperty.get());
        entity.setStudentId(this.studentIdProperty.get());
        entity.setStudentId(entity.getStudentId());
        entity.setName(this.semesterNameProperty.get());
        return super.updateEntity();
    }

    @Override
    public void setEntity(Semester entity) {
        super.setEntity(entity);
        this.semesterIdProperty = new SimpleLongProperty(entity.getSemesterId());
        this.studentIdProperty = new SimpleLongProperty(entity.getStudentId());
        this.studentIdProperty = new SimpleLongProperty(entity.getStudentId());
        this.semesterNameProperty = new SimpleStringProperty(entity.getName());
        
        List<Module> modules = entity.getModules();
        
        if (modules != null && !modules.isEmpty()) {
            for (Module module : modules) {
                this.moduleData.add(new ModuleBean(module));
            }
        }
    }

	public LongProperty getSemesterIdProperty() {
		return semesterIdProperty;
	}

	public void setSemesterIdProperty(LongProperty semesterIdProperty) {
		this.semesterIdProperty = semesterIdProperty;
	}

}
