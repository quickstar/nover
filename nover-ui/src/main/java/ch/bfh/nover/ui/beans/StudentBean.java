package ch.bfh.nover.ui.beans;

import java.util.Iterator;
import java.util.List;

import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.Student;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class StudentBean extends EntityBean<Student> {
    private LongProperty studentIdProperty;

    private StringProperty firstNameProperty;

    private StringProperty lastNameProperty;

    private StringProperty userNameProperty;

    private StringProperty passwordProperty;

    private ObservableList<SemesterBean> semesterData = FXCollections.observableArrayList();

    public StudentBean() {
        setEntity(new Student());
    }

    public StudentBean(Student student) {
        setEntity(student);
    }

    public StudentBean(String firstName, String lastName, String userName, String password) {
        setEntity(new Student(firstName, lastName, userName, password));
    }

    // studentID Getters and Setters
    public Long getStudentId() {
        return studentIdProperty.get();
    }

    public void setStudentId(Long studentId) {
        this.studentIdProperty.set(studentId);
    }

    public LongProperty getStudentIdProperty(Long studentId) {
        return studentIdProperty;
    }

    // firstName Getters and Setters
    public String getFirstname() {
        return firstNameProperty.get();
    }

    public void setFirstname(String firstName) {
        this.firstNameProperty.set(firstName);
        ;
    }

    public StringProperty getFirstNameProperty(Long studentId) {
        return firstNameProperty;
    }

    // lastName Getters and Setters
    public String getLastname() {
        return lastNameProperty.get();
    }

    public void setLastname(String lastName) {
        this.lastNameProperty.set(lastName);
        ;
    }

    public StringProperty getLastNameProperty(Long studentId) {
        return lastNameProperty;
    }

    // userName Getters and Setters
    public String getUsername() {
        return userNameProperty.get();
    }

    public void setUsername(String lastName) {
        this.userNameProperty.set(lastName);
        ;
    }

    public StringProperty getUserNameProperty(Long studentId) {
        return userNameProperty;
    }

    // password Getters and Setters
    public String getPassword() {
        return passwordProperty.get();
    }

    public void setPassword(String lastName) {
        this.passwordProperty.set(lastName);
        ;
    }

    public StringProperty getPasswordProperty(Long studentId) {
        return passwordProperty;
    }

    // to get the Index of the chosen Semester
    public int getIndexOfSem(SemesterBean semester) {
        for (int i = 0; i < semesterData.size(); i++) {
            if (semester.getSemesterName().equals(semesterData.get(i).getSemesterName())) {
                return i;
            }
        }
        return 0;
    }

    // to get the Index of the chosen Module
    public int getIndexOfMod(SemesterBean semester, ModuleBean module) {
        int indexOfSem = getIndexOfSem(semester);
        for (int i = 0; i < semesterData.get(indexOfSem).getModuleData().size(); i++) {
            if (module.getModuleName().equals(semesterData.get(indexOfSem).getModuleData().get(i).getModuleName())) {
                return i;
            }
        }
        return 0;
    }

    // to get the Index of the chosen Test
    public int getIndexOfTest(SemesterBean semester, ModuleBean module, MarkBean test) {
        int indexOfSem = getIndexOfSem(semester);
        int indexOfMod = getIndexOfMod(semester, module);
        for (int i = 0; i < semesterData.get(indexOfSem).getModuleData().get(indexOfMod).getTests().size(); i++) {
            if (module.getModuleName().equals(
                    semesterData.get(indexOfSem).getModuleData().get(indexOfMod).getTests().get(i).getTestName())) {
                return i;
            }
        }
        return 0;
    }

    // Semester Methods
    public ObservableList<SemesterBean> getSemesterData() {
        return semesterData;
    }

    public void addNewSemester(SemesterBean newSemesterBean) {
        semesterData.add(newSemesterBean);
    }

    public void deleteSemester(SemesterBean semester) {
        int indexOfSem = getIndexOfSem(semester);
        semesterData.remove(indexOfSem);
    }

    // Module Methods
    public void addNewModuleIntoStruct(SemesterBean semester, ModuleBean module) {
        int indexOfSem = getIndexOfSem(semester);
        semesterData.get(indexOfSem).addNewModule(module);
    }

    public ObservableList<ModuleBean> getModuleDataFromStruct(SemesterBean semester) {
        int indexOfSem = getIndexOfSem(semester);
        return semesterData.get(indexOfSem).getModuleData();
    }

    public void deleteModule(SemesterBean semester, ModuleBean module) {
        int indexOfSem = getIndexOfSem(semester);
        int indexOfMod = getIndexOfMod(semester, module);
        semesterData.get(indexOfSem).getModuleData().remove(indexOfMod);
    }

    // Test Methods
    public void addNewTestIntoStruct(SemesterBean semester, ModuleBean module, MarkBean newMarkBean) {
        int indexOfSem = getIndexOfSem(semester);
        int indexOfMod = getIndexOfMod(semesterData.get(indexOfSem), module);
        semesterData.get(indexOfSem).getModuleData().get(indexOfMod).addNewTest(newMarkBean);
    }

    public ObservableList<MarkBean> getTestDataFromStruct(SemesterBean semester, ModuleBean module) {
        int indexOfSem = getIndexOfSem(semester);
        int indexOfMod = getIndexOfMod(semesterData.get(indexOfSem), module);
        return semesterData.get(indexOfSem).getModuleData().get(indexOfMod).getTests();
    }

    public void deleteTest(SemesterBean semester, ModuleBean module, MarkBean test) {
        for (SemesterBean currentSem : semesterData) {
            ObservableList<ModuleBean> moduleData = currentSem.getModuleData();
            for (ModuleBean modulebean : moduleData) {
                ObservableList<MarkBean> tests = modulebean.getTests();
                Iterator<MarkBean> iterator = tests.iterator();
                while (iterator.hasNext()) {
                    MarkBean current = iterator.next();
                    
                    if (test.getMarkIdProperty().get() == current.getMarkIdProperty().get()) {
                        iterator.remove();
                    }
                }
            }
        }
    }

    @Override
    public Student updateEntity() {
        Student entity = getEntity();
        entity.setStudentId(this.studentIdProperty.get());
        entity.setFirstname(this.firstNameProperty.get());
        entity.setLastname(this.lastNameProperty.get());
        entity.setPassword(this.passwordProperty.get());
        entity.setUsername(this.userNameProperty.get());
        return super.updateEntity();
    }

    @Override
    public void setEntity(Student entity) {
        super.setEntity(entity);
        this.studentIdProperty = new SimpleLongProperty(entity.getStudentId());
        this.firstNameProperty = new SimpleStringProperty(entity.getFirstname());
        this.lastNameProperty = new SimpleStringProperty(entity.getLastname());
        this.passwordProperty = new SimpleStringProperty(entity.getPassword());
        this.userNameProperty = new SimpleStringProperty(entity.getUsername());
        
        // semester abfüllen
        List<Semester> semesters = entity.getSemesters();
        if (semesters != null && !semesters.isEmpty()) {
            for (Semester sem : semesters) {
                this.semesterData.add(new SemesterBean(sem));
            }
        }
    }
}
