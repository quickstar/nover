package ch.bfh.nover.ui;

import com.google.inject.Inject;

import ch.bfh.nover.context.UserContext;
import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Student;
import ch.bfh.nover.service.UserService;
import ch.bfh.nover.util.CryptoManager;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;


public class LoginController implements MainAppProvider {
	
	private UserService userService;

	@FXML
    private TextField userName;
	@FXML
    private PasswordField password;
	@FXML
	private Button	loginBtn;
	@FXML
	private Hyperlink registerLink;
	
	private MainApp mainApp;
	private UserContext userContext;

	@Inject
	public LoginController(UserService userService, UserContext userContext) {
		this.userService = userService;
		this.userContext = userContext;
	}

	@FXML
	private void getLoginInputs(){
		Credentials credentials = new Credentials();
		credentials.setUsername(userName.getText());
		credentials.setPassword(password.getText());
		Student registeredStudent = userService.login(credentials);

		if (registeredStudent != null) {
			userContext.setFromStudent(registeredStudent);
			mainApp.replaceSceneContent("Application.fxml");
		}
		else
		{
			// TODO: Add error handling
		}
	}

	@Override
	public void addMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void showRegister(Event event) {
		mainApp.replaceSceneContent("Register.fxml");
	}
}
