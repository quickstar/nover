package ch.bfh.nover.ui;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.IOException;

public class MainApp extends Application {

    private Stage _mainStage;

    private Injector injector;

    private JavaFXBuilderFactory _javaFxBuilderFactory = new JavaFXBuilderFactory();

    private MainApp _currentInstance;

    @Inject
    public MainApp() {
        injector = Guice.createInjector(new AppInjector());
    }

    private Callback<Class<?>, Object> _guiceControllerFactory = new Callback<Class<?>, Object>() {
        @Override
        public Object call(Class<?> clazz) {
            Object controller = injector.getInstance(clazz);
            if (controller instanceof MainAppProvider) {
                MainAppProvider mainAppProvider = (MainAppProvider)controller;
                mainAppProvider.addMainApp(_currentInstance);
            }
            return controller;
        }
    };

    @Override
    public void start(Stage primaryStage) {
        _currentInstance = this;

        this._mainStage = primaryStage;
        this._mainStage.setTitle("NOVER The Perfect Grade managing Tool ");
        replaceSceneContent("Login.fxml");
        _mainStage.show();
    }


    public Parent replaceSceneContent(String fxml) {
        FXMLLoader loader = new FXMLLoader(MainApp.class.getResource(fxml), null, _javaFxBuilderFactory, _guiceControllerFactory);
        Parent pageToLoad = null;
        try {
            pageToLoad = loader.load();

            Object controller = loader.getController();
            if (controller instanceof MasterPage){
                FXMLLoader masterPageLoader = new FXMLLoader();
                masterPageLoader.setLocation(MainApp.class.getResource("RootLayout.fxml"));
                BorderPane masterPage = masterPageLoader.load();

                masterPage.setCenter(pageToLoad);
                pageToLoad = masterPage;
            }

            Scene scene = _mainStage.getScene();
            if (scene == null) {
                scene = new Scene(pageToLoad);
                _mainStage.setScene(scene);
            } else {
                _mainStage.getScene().setRoot(pageToLoad);
            }
            _mainStage.sizeToScene();
        } catch (IOException e) {
            System.out.printf(e.getMessage());
        }
        return pageToLoad;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
