package ch.bfh.nover.ui;

import ch.bfh.nover.context.UserContext;
import ch.bfh.nover.context.UserContextImpl;
import ch.bfh.nover.service.*;
import com.google.inject.AbstractModule;

import ch.bfh.nover.db.util.PersistanceManager;
import ch.bfh.nover.db.util.PersistanceManagerImpl;
import ch.bfh.nover.service.client.NoverClient;
import ch.bfh.nover.service.client.NoverClientImpl;

public class AppInjector extends AbstractModule{
    @Override
    protected void configure() {
        // Services
        bind(NotenService.class).to(NotenServiceImpl.class);
        bind(UserService.class).to(UserServiceImpl.class);
        bind(NoverClient.class).to(NoverClientImpl.class);
        bind(UserContext.class).to(UserContextImpl.class).asEagerSingleton();
        
        // Persistance
        bind(PersistanceManager.class).to(PersistanceManagerImpl.class);
    }
}
