package ch.bfh.nover.ui.beans;

public abstract class EntityBean<T> {

    private T entity;

    public T updateEntity() {
        return entity;
    };

    public void setEntity(final T entity){
        this.entity = entity;
    }

    public T getEntity() {
        return entity;
    }
}
