package ch.bfh.nover.ui;


import ch.bfh.nover.context.UserContext;
import ch.bfh.nover.domain.Credentials;
import com.google.inject.Inject;

import ch.bfh.nover.domain.Student;
import ch.bfh.nover.service.UserService;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;


public class RegisterController implements MainAppProvider {
	
	private final UserService userService;
	private UserContext userContext;

	@FXML
    private TextField firstName;
	@FXML
    private TextField secondName;
	@FXML
    private TextField userName;
	@FXML
    private PasswordField password1;
	@FXML
    private PasswordField password2;
	@FXML
	private Button	registerBtn;
	@FXML
	private Hyperlink loginLink;
	
	private MainApp mainApp;
	
	@Inject
	public RegisterController(UserService userService, UserContext userContext) {
		this.userService = userService;
		this.userContext = userContext;
	};
	
	public UserService getUserService() {
		return userService;
	}
	
	@FXML
	private void getRegisterInputs() {
		Student student = new Student();
		student.setFirstname(firstName.getText());
		student.setLastname(secondName.getText());
		student.setUsername(userName.getText());
		String code1 = (password1.getText());
		String code2 = (password2.getText());

		if (code1.equals(code2)) {
			student.setPassword(code2);
		}
		
		student = userService.register(student);
		if (student != null)
		{
			Credentials credentials = new Credentials();
			credentials.setUsername(student.getUsername());
			credentials.setPassword(student.getPassword());
			userContext.setFromStudent(student);
			mainApp.replaceSceneContent("Application.fxml");
		} else {
			// TODO: print error Message
		}
	}

	@Override
	public void addMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public void showLoginWindow(Event event) {
		mainApp.replaceSceneContent("Login.fxml");
	}
}
