package ch.bfh.nover.ui.beans;

import java.util.List;

import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ModuleBean extends EntityBean<Module>{
    private ObservableList<MarkBean> tests = FXCollections.observableArrayList();

    private LongProperty moduleIdProperty;

    private LongProperty semesterIdProperty;
    
    private StringProperty moduleNameProperty;

    private StringProperty moduleTargGradeProperty;

    public ModuleBean() {
        setEntity(new Module());
    }
   ;
    public ModuleBean(Module module) {
        setEntity(module);
    }

    public ModuleBean(String moduleName) {
        setEntity(new Module(moduleName));
    }

    public LongProperty getModuleIdProperty() {
		return moduleIdProperty;
	}
	public void setModuleIdProperty(LongProperty moduleIdProperty) {
		this.moduleIdProperty = moduleIdProperty;
	}
	public LongProperty getSemesterIdProperty() {
		return semesterIdProperty;
	}
	public void setSemesterIdProperty(LongProperty semesterIdProperty) {
		this.semesterIdProperty = semesterIdProperty;
	}
	// moduleNameProperty Getters and Setters
    public void setModuleName(String moduleName) {
        moduleNameProperty.set(moduleName);
    }

    public String getModuleName() {
        return moduleNameProperty.get();
    }

    public StringProperty getModuleNameProperty() {
        return moduleNameProperty;
    }

    // gradeProperty Getters and Setters
    public void setModuleGrade(String grade) {
        moduleTargGradeProperty.set(grade);
    }

    public String getModuleGrade() {
        return moduleTargGradeProperty.get();
    }

    public StringProperty getModuleGradeProperty() {

        return moduleTargGradeProperty;
    }

    public ObservableList<MarkBean> getTests() {
        return tests;
    }

    public void addNewTest(MarkBean newMarkBean) {
        tests.add(newMarkBean);
    }

    public void removeTest(int index) {
        this.tests.remove(index);
    }

    @Override
    public Module updateEntity() {
        Module entity = getEntity();
        entity.setModuleId(this.moduleIdProperty.get());
        entity.setSemesterId(this.semesterIdProperty.get());
        entity.setName(this.moduleNameProperty.get());
        return super.updateEntity();
    }
    
    @Override
    public void setEntity(Module entity) {
        super.setEntity(entity);
        this.moduleIdProperty = new SimpleLongProperty(entity.getModuleId());
        this.semesterIdProperty= new SimpleLongProperty(entity.getSemesterId());
        this.moduleNameProperty= new SimpleStringProperty(entity.getName());
        
        List<Mark> marks = entity.getMarks();
        if (marks != null && !marks.isEmpty()) {
            for (Mark mark : marks) {
                this.tests.add(new MarkBean(mark));
            }
        }
    }
}
