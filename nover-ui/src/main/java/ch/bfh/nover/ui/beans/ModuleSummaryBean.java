package ch.bfh.nover.ui.beans;

import ch.bfh.nover.domain.ModuleSummary;

public class ModuleSummaryBean extends EntityBean<ModuleSummary> {
	private boolean sufficient;
	private String mark;
	
	
	public ModuleSummaryBean(ModuleSummary moduleSummary) {
	    setEntity(moduleSummary);
	}
	
	public ModuleSummaryBean() {
	    setEntity(new ModuleSummary());
	}
	
	public boolean isSufficient() {
		return sufficient;
	}
	public void setSufficient(boolean sufficient) {
		this.sufficient = sufficient;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
	
    @Override
    public ModuleSummary updateEntity() {
        ModuleSummary entity = getEntity();
        entity.setMark(this.mark);
        entity.setSufficent(this.sufficient);
        return super.updateEntity();
    }
    
    @Override
    public void setEntity(ModuleSummary entity) {
        super.setEntity(entity);
        this.sufficient = entity.isSufficent();
        this.mark = entity.getMark();
    }
}
