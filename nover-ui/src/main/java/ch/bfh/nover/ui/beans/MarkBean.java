package ch.bfh.nover.ui.beans;

import ch.bfh.nover.domain.Mark;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MarkBean extends EntityBean<Mark> {

    private LongProperty markIdProperty;

    private LongProperty moduleIdProperty;

    private StringProperty testNameProperty;

    private StringProperty testGradeProperty;

    private IntegerProperty testWeightProperty;

    private StringProperty testRecomProperty;

    public MarkBean() {

    }

    public MarkBean(Mark mark) {
        setEntity(mark);
    }

    public MarkBean(String testName, String testGrade, Integer testWeight, String testRecom) {
        setEntity(new Mark(testName, testGrade, testWeight));
    }

    // testNameProperty Getters and Setters
    public void setTestName(String testName) {
        testNameProperty.set(testName);
    }

    public String getTestName() {
        return testNameProperty.get();
    }

    public StringProperty getTestNameProperty() {

        return testNameProperty;
    }

    // testGradeProperty Getters and Setters
    public void setTestGrade(String testGrade) {
        testGradeProperty.set(testGrade);
    }

    public String getTestGrade() {
        return testGradeProperty.get();
    }

    public StringProperty getTestGradeProperty() {

        return testGradeProperty;
    }

    // testWeightProperty Getters and Setters
    public void setTestWeight(Integer testWeight) {
        testWeightProperty.set(testWeight);
    }

    public Integer getTestWeight() {
        return testWeightProperty.get();
    }

    public IntegerProperty getTestWeightProperty() {
        return testWeightProperty;
    }

    // testRecomProperty Getters and Setters
    public void setTestRecom(String testRecom) {
        testRecomProperty.set(testRecom);
    }

    public String getTestRecom() {
        return testRecomProperty.get();
    }

    public LongProperty getMarkIdProperty() {
		return markIdProperty;
	}

	public void setMarkIdProperty(LongProperty markIdProperty) {
		this.markIdProperty = markIdProperty;
	}

	public LongProperty getModuleIdProperty() {
		return moduleIdProperty;
	}

	public void setModuleIdProperty(LongProperty moduleIdProperty) {
		this.moduleIdProperty = moduleIdProperty;
	}

	public StringProperty getTestRecomProperty() {

        return testRecomProperty;
    }

    @Override
    public Mark updateEntity() {
        Mark entity = getEntity();
        entity.setMarkId(this.markIdProperty.get());
        entity.setModuleId(this.moduleIdProperty.get());
        entity.setName(this.testNameProperty.get());
        entity.setMark(this.testGradeProperty.get());
        entity.setValidity(this.testWeightProperty.get());
        return super.updateEntity();
    }

    @Override
    public void setEntity(Mark entity) {
        super.setEntity(entity);
        this.markIdProperty = new SimpleLongProperty(entity.getMarkId());
        this.moduleIdProperty = new SimpleLongProperty(entity.getModuleId());
        this.testNameProperty = new SimpleStringProperty(entity.getName());
        this.testGradeProperty = new SimpleStringProperty(entity.getMark());
        this.testWeightProperty = new SimpleIntegerProperty(entity.getValidity());
    }
}
