package ch.bfh.nover.ui;

import java.net.URL;
import java.util.ResourceBundle;

import com.google.inject.Inject;
import com.mchange.v2.lang.StringUtils;

import ch.bfh.nover.context.UserContext;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.ModuleSummary;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.service.NotenService;
import ch.bfh.nover.ui.beans.MarkBean;
import ch.bfh.nover.ui.beans.ModuleBean;
import ch.bfh.nover.ui.beans.SemesterBean;
import ch.bfh.nover.ui.beans.StudentBean;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class ApplicationController implements Initializable,MasterPage,MainAppProvider {
    private final UserContext _userContext;

    private MainApp mainApp;

    @FXML
    private Label userInfo = new Label();

    StudentBean _studentBean;

    private NotenService notenService;

    // Semester Elements Declaration
    @FXML
    private TableView<SemesterBean> semesterTable;

    @FXML
    private TableColumn<SemesterBean, String> semesterColumn;

    @FXML
    private TextField newSemesterNameField;

    @FXML
    private Button semesterAddBtn;

    @FXML
    private Button semesterDeleteBtn;

    // Module Elements Declaration
    @FXML
    private TableView<ModuleBean> moduleTable;

    @FXML
    private TableColumn<ModuleBean, String> moduleNameColumn;

    @FXML
    private TableColumn<ModuleBean, String> moduleGradeColumn;

    @FXML
    private TextField newModuleNameField;

    @FXML
    private TextField newModuleGradeField;

    @FXML
    private Button moduleAddBtn;

    @FXML
    private Button moduleDeleteBtn;

    // Test Elements Declaration
    @FXML
    private TableView<MarkBean> testTable;

    @FXML
    private TableColumn<MarkBean, String> testNameColumn;

    @FXML
    private TableColumn<MarkBean, Integer> testWeightColumn;

    @FXML
    private TableColumn<MarkBean, String> testGradeColumn;

    @FXML
    private TableColumn<MarkBean, String> testRecomColumn;

    @FXML
    private TextField newTestNameField;

    @FXML
    private TextField newTestGradeField;

    @FXML
    private TextField newTestWeightField;

    @FXML
    private TextField newTestRecomField;

    @FXML
    private Button testDeleteBtn;

    @FXML
    private Button testAddBtn;

    // Other Declarations
    @FXML
    private Label feedBackLabel;
    
    @FXML
    private Label summaryLabel;

    @Inject
    public ApplicationController(NotenService notenService, UserContext userContext) {
        this.notenService = notenService;
        
        // initialize tablesR
        this.moduleTable = new TableView<>();
        this.testTable = new TableView<>();
        
        _userContext = userContext;
    }

    private void showModules(SemesterBean semester) {
        if (semester != null) {
            moduleTable.setItems(semester.getModuleData());
        }
    }

    private void showTests(ModuleBean module) {
        if (module != null) {
            testTable.setItems(module.getTests());
            ModuleSummary moduleSummary = notenService.getModuleSummary(module.updateEntity());
            summaryLabel.setText(moduleSummary.getMark());
            summaryLabel.setStyle(moduleSummary.isSufficent() ? "-fx-text-fill: green" : "-fx-text-fill: red");
        }
    }

    public void addSemester() {
        hideErrorMessage();
        String newSemesterName = newSemesterNameField.getText();
        if (!StringUtils.nonEmptyString(newSemesterName)) {
            showErrorMessage("Semestername leer");
            return;
        }
        Semester semester = new Semester(newSemesterName);
        semester.setStudentId(_userContext.getUserId());
        Semester addedSemester = notenService.addSemester(_userContext.getCredentials(), semester);
        if (addedSemester == null) {
            showErrorMessage("Semester konnte nicht hinzugefügt werden.");
            return;
        }

//        semesterTable.getItems().add(new SemesterBean(addedSemester));
        _studentBean.addNewSemester(new SemesterBean(addedSemester));
    }

    public void deleteSemester() {
        hideErrorMessage();
        SemesterBean selectedSem = getCurrentSelectedSem();

        if (!notenService.deleteSemester(_userContext.getCredentials(), selectedSem.getSemesterIdProperty().get())) {
            showErrorMessage("Semester konnte nicht gelöscht werden.");
            return;
        }
        hideErrorMessage();
        _studentBean.getSemesterData().remove(selectedSem);
    }

    public void addNewModule() {
        hideErrorMessage();
        String newModuleName = newModuleNameField.getText();
        if (!StringUtils.nonEmptyString(newModuleName)) {
            showErrorMessage("Module Name leer");
            return;
        }

        if (null == getCurrentSelectedSem()) {
            showErrorMessage("Semester not selected");
            return;
        }
        // Module erstellen mit Name und ausgewählten Semester als SemesterID
        Module newModule = new Module(newModuleName);
        newModule.setSemesterId(getCurrentSelectedSem().getSemesterIdProperty().get());
        Module addedModule = notenService.addModule(_userContext.getCredentials(), newModule);

        if (addedModule == null) {
            showErrorMessage("Module konnte nicht hinzugefügt werden.");
            return;
        }

        _studentBean.addNewModuleIntoStruct(getCurrentSelectedSem(), new ModuleBean(addedModule));
    }

    public void deleteModule() {
        ModuleBean selectedMod = getCurrentSelectedMod();
        if (selectedMod == null) {
            showErrorMessage("Kein Module ausgewählt");
            return;
        }

        if (!notenService.deleteModule(_userContext.getCredentials(), selectedMod.getModuleIdProperty().get())) {
            showErrorMessage("Module konnte nicht entfernt werden");
            return;
        }
        
        hideErrorMessage();
        _studentBean.deleteModule(getCurrentSelectedSem(), selectedMod);

    }

    public void addNewTest() {
        hideErrorMessage();

        String newTestName = newTestNameField.getText();
        String newTestGrade = newTestGradeField.getText();
        int newTestWeight;
        try {
            newTestWeight = Integer.parseInt(newTestWeightField.getText());
        } catch (Exception c) {
            showErrorMessage("Please enter a Number in Weight Field");
            return;
        }

        if ((!StringUtils.nonEmptyString(newTestName)) && (!StringUtils.nonEmptyString(newTestGrade))
                && (0 != newTestWeight)) {
            showErrorMessage("Test Name,Grade oder Weight leer");
            return;
        }

        if (null == getCurrentSelectedMod()) {
            showErrorMessage("Module not selected");
            return;
        }
        // Module erstellen mit Name und ausgewählten Semester als SemesterID
        Mark newMark = new Mark(newTestName, newTestGrade, newTestWeight);
        long currentModuleId = getCurrentSelectedMod().getModuleIdProperty().get();
        newMark.setModuleId(currentModuleId);
        
        
        Mark addedMark = notenService.addMark(_userContext.getCredentials(), newMark);

        if (addedMark == null) {
            showErrorMessage("Module konnte nicht hinzugefügt werden.");
            return;
        }

        _studentBean.addNewTestIntoStruct(getCurrentSelectedSem(), getCurrentSelectedMod(), new MarkBean(addedMark));

    }

    public void deleteTest() {
        hideErrorMessage();
        MarkBean selectedMark = getCurrentSelectedMark();
        if (selectedMark == null) {
            showErrorMessage("Kein Test ausgewählt");
            return;
        }

        if (!notenService.deleteMark(_userContext.getCredentials(), selectedMark.getMarkIdProperty().get())) {
            showErrorMessage("Test konnte nicht gelöscht werden");
            return;
        }
        _studentBean.deleteTest(getCurrentSelectedSem(), getCurrentSelectedMod(), selectedMark);
    }

    private SemesterBean getCurrentSelectedSem() {
        ReadOnlyObjectProperty<SemesterBean> selectedItemProperty = semesterTable.getSelectionModel()
                .selectedItemProperty();
        if (selectedItemProperty != null) {
            return selectedItemProperty.get();
        }
        return null;
    }

    private ModuleBean getCurrentSelectedMod() {
        ReadOnlyObjectProperty<ModuleBean> selectedItemProperty = moduleTable.getSelectionModel()
                .selectedItemProperty();
        if (selectedItemProperty != null) {
            return selectedItemProperty.get();
        }
        return null;
    }

    private MarkBean getCurrentSelectedMark() {
        ReadOnlyObjectProperty<MarkBean> selectedMark = testTable.getSelectionModel().selectedItemProperty();
        if (selectedMark != null) {
            return selectedMark.get();
        }
        return null;
    }

    private void hideErrorMessage() {
        feedBackLabel.setVisible(false);
    }

    private void showErrorMessage(String errorMessage) {
        feedBackLabel.setText(errorMessage);
        feedBackLabel.setVisible(true);
    }

    @Override
    public void addMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    private void initialize() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        userInfo.setText(_userContext.getFirstName() + " " + _userContext.getLastName());

        // Die Aktualisierung der Zellen SemesterName
        semesterColumn.setCellValueFactory(cellData -> cellData.getValue().getSemesterNameProperty());
        // Die Aktualisierung der Zellen ModuleName und ModuleGrade
        moduleNameColumn.setCellValueFactory(cellData -> cellData.getValue().getModuleNameProperty());
        // Die Aktualisierung der Zellen im Tests TableView
        testNameColumn.setCellValueFactory(cellData -> cellData.getValue().getTestNameProperty());
        testGradeColumn.setCellValueFactory(cellData -> cellData.getValue().getTestGradeProperty());
        testWeightColumn.setCellValueFactory(cellData -> cellData.getValue().getTestWeightProperty().asObject());
        // Listener ruft die showModules Methode mit dem neu ausgewählten Semester als Parameter
        semesterTable.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showModules(newValue));
        // Listener ruft die showTests Methode mit dem neu ausgewählten module als Parameter
        moduleTable.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showTests(newValue));
        _studentBean = new StudentBean(_userContext.getStudent());
        semesterTable.setItems(_studentBean.getSemesterData());
    }
}
