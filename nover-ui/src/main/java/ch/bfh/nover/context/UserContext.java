package ch.bfh.nover.context;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Student;

/**
 * Created by ll0151 on 08/01/16.
 */
public interface UserContext {
    long getUserId();

    String getFirstName();

    String getLastName();

    String getUsername();

    Student getStudent();

    Credentials getCredentials();

    void setFromStudent(Student student);
}
