package ch.bfh.nover.context;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Student;

/**
 * Created by ll0151 on 08/01/16.
 */
public class UserContextImpl implements UserContext {
    private long _userId;
    private String _firstName;
    private String _lastName;
    private String _username;
    private Student _student;
    private Credentials _credentials;

    public long getUserId() {
        return _student.getStudentId();
    }

    public String getFirstName() {
        return _student.getFirstname();
    }

    public String getLastName() {
        return _student.getLastname();
    }

    public String getUsername() {
        return _student.getUsername();
    }

    public Student getStudent() { return _student; }

    @Override
    public Credentials getCredentials() {
        return _credentials;
    }

    @Override
    public void setFromStudent(Student student) {
        _userId = student.getStudentId();
        _firstName = student.getFirstname();
        _lastName = student.getLastname();
        _username = student.getUsername();
        _student = student;

        _credentials = new Credentials();
        _credentials.setPassword(student.getPassword());
        _credentials.setUsername(student.getUsername());
    }

    @Override
    public String toString() {
        return "UserContextImpl [_userId=" + _userId + ", _firstName=" + _firstName + ", _lastName=" + _lastName
                + ", _username=" + _username + "]";
    }
}
