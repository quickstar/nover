package ch.bfh.nover.domain.service;

public enum BolognaMark {
    A(100f),
    
    B(91.6f), 
    
    C(83.3f), 
    
    D(75.0f), 
    
    E(66.6f), 
    
    F(58.3f);
    
    private float percentage;
    
    private BolognaMark(float mark) {
        this.percentage = mark;
    }

    public float getPercentage() {
        return percentage;
    }
}
