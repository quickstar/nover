package ch.bfh.nover.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Mark {

    @Id
    @GeneratedValue
    private long markId;
    
    private long moduleId;

    private String name;

    private String mark;

    private Integer validity;

    public Mark() {
        // empty constructor
    }

    public Mark(String name, String mark, Integer validity) {
        this.mark = mark;
        this.validity = validity;
        this.name = name;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Mark [markId=" + markId + ", name=" + name + ", mark=" + mark + ", validity=" + validity + "]";
    }

    public Long getMarkId() {
        return markId;
    }

    public void setMarkId(Long markId) {
        this.markId = markId;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

}
