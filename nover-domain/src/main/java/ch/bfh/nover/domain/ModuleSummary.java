package ch.bfh.nover.domain;

public class ModuleSummary {
    
    private String mark;
    
    private boolean sufficent;

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public boolean isSufficent() {
        return sufficent;
    }

    public void setSufficent(boolean sufficent) {
        this.sufficent = sufficent;
    }

}
