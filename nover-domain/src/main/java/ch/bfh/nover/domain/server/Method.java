package ch.bfh.nover.domain.server;

import java.io.Serializable;

import ch.bfh.nover.domain.Credentials;
import ch.bfh.nover.domain.Mark;
import ch.bfh.nover.domain.Module;
import ch.bfh.nover.domain.Semester;
import ch.bfh.nover.domain.Student;

public enum Method implements Serializable {
    REGISTER_STUDENT(Student.class, Student.class),

    LOGIN(Credentials.class, Student.class),

    ADD_SEMESTER(Semester.class, Semester.class),

    ADD_MODULE(Module.class, Module.class),

    ADD_MARK(Mark.class, Mark.class),

    DELETE_STUDENT(Long.class, Boolean.class), 
    
    DELETE_SEMESTER(Long.class, Boolean.class), 
    
    DELETE_MODULE(Long.class, Boolean.class), 
    
    DELETE_MARK(Long.class, Boolean.class);

    private Class requestClazz;

    private Class responseClazz;

    private boolean isList;

    private Method(Class requestClazz, Class responseClazz, boolean isList) {
        this.requestClazz = requestClazz;
        this.responseClazz = responseClazz;
        this.isList = isList;
    }

    private Method(Class requestClazz, Class responseClazz) {
        this(requestClazz, responseClazz, false);
    }

    public Class getRequestClazz() {
        return requestClazz;
    }

    public void setRequestClazz(Class requestClazz) {
        this.requestClazz = requestClazz;
    }

    public Class getResponseClazz() {
        return responseClazz;
    }

    public void setResponseClazz(Class responseClazz) {
        this.responseClazz = responseClazz;
    }

    public boolean isList() {
        return isList;
    }

    public void setList(boolean isList) {
        this.isList = isList;
    }
}
