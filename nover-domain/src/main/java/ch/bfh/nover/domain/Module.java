package ch.bfh.nover.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Module {

    @Id
    @GeneratedValue
    private long moduleId;
    
    private long semesterId;
    
    private String name;
    
    @OneToMany(mappedBy = "moduleId")
    List<Mark> marks;
    
    public Module() {
        // empty constuctor
    }
    
    public Module(String name) {
        this.name = name;
    }
    
    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(Long semesterId) {
        this.semesterId = semesterId;
    }

    @Override
    public String toString() {
        return "Module [moduleId=" + moduleId + ", semesterId=" + semesterId + ", name=" + name + ", marks=" + marks
                + "]";
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public void setSemesterId(long semesterId) {
        this.semesterId = semesterId;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

}
