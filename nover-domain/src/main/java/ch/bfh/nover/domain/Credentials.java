package ch.bfh.nover.domain;

import ch.bfh.nover.util.CryptoManager;

public class Credentials {

    private String username;
    
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = CryptoManager.hash(password);
    }
    @Override
    public String toString() {
        return "Credentials [username=" + username + ", password=" + password + "]";
    }
}
