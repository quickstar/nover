package ch.bfh.nover.domain;

import ch.bfh.nover.util.CryptoManager;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Student {

    @Id
    @GeneratedValue
    private long studentId;

    private String firstname;

    private String lastname;

    private String username;

    private String password;

    @OneToMany(mappedBy = "studentId")
    private List<Semester> semesters;

    public Student() {
        // empty constructor
    }

    public Student(String firstname, String lastname, String username, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = CryptoManager.hash(password);
    }

    @Override
    public String toString() {
        return "Student [studentId=" + studentId + ", firstname=" + firstname + ", lastname=" + lastname + ", username="
                + username + ", password=" + password + ", semesters=" + semesters + "]";
    }

    public List<Semester> getSemesters() {
        return semesters;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }
}
