package ch.bfh.nover.domain.server;

public class ServerResponse {
    private Status _status;

    private String _payload;

    private Method _method;

    public Method getMethod() {
        return _method;
    }

    public void setMethod(Method _method) {
        this._method = _method;
    }

    public String getPayload() {
        return _payload;
    }

    public void setPayload(String _payload) {
        this._payload = _payload;
    }

    public Status getStatus() {
        return _status;
    }

    public void setStatus(Status _status) {
        this._status = _status;
    }
}
