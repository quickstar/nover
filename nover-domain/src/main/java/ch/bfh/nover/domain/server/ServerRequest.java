package ch.bfh.nover.domain.server;

import ch.bfh.nover.domain.Credentials;

import java.io.Serializable;

public class ServerRequest implements Serializable {
    private Method method;

    private String payload;

    private Credentials credentials;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }
}
