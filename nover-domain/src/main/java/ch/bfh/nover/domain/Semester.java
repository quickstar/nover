package ch.bfh.nover.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Semester {
    
    @Id
    @GeneratedValue
    private long semesterId;
    
    private long studentId;
    
    private String name;
    
    @OneToMany(mappedBy = "semesterId")
    private List<Module> modules;
    
    
    public Semester() {
        // emtpy constructor
    }
    
    public Semester(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(Long semesterId) {
        this.semesterId = semesterId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "Semester [semesterId=" + semesterId + ", studentId=" + studentId + ", name=" + name + ", modules="
                + modules + "]";
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setSemesterId(long semesterId) {
        this.semesterId = semesterId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }
}
